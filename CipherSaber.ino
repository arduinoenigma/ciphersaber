// Arduino CipherSaber-2 (http://www.ciphersaber.com)
// probably_random code from https://gist.github.com/endolith/2568571
//
// @arduinoenigma 2019


#include <stdint.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <TimerOne.h>


#define MAXRANDOMSAMPLES 10


struct PROBABLYRANDOM_t
{
  byte sample = 0;
  boolean sample_waiting = false;
  byte current_bit = 0;
  byte result = 0;

  byte random[MAXRANDOMSAMPLES];
  byte randomptr = 0;
  byte samples = 0;

} PROBABLYRANDOM;


struct ARCFOUR_t     /*xls*/
{
  byte IV[10];
  byte Key[256];
  byte KeyEnd = 0;
  byte State[256];
  byte i = 0;
  byte j = 0;
  byte n = 0;
} ARCFOUR;


struct MenuData_t
{
  byte State = 0;
  byte NextState = 0;
  byte SkipNewline = 0;
  byte Rounds[2];
  byte PrintIV = 0;
  byte PrintCount = 0;
  byte ExitCount = 0;
  byte IVRead = 0;
  byte Hex[2];
  byte HexRead = 0;
  byte DecodeAsHex = 0;
} MenuData;


// Watchdog Timer Interrupt
ISR(WDT_vect)
{
  PROBABLYRANDOM.sample = TCNT1L; // Ignore higher bits
  TCNT1 = 0; // Clear Timer 1
  PROBABLYRANDOM.sample_waiting = true;
}


// Setup of the watchdog timer.
void PROBABLYRANDOMSetup() {
  cli();
  MCUSR = 0;

  /* Start timed sequence */
  WDTCSR |= _BV(WDCE) | _BV(WDE);

  /* Put WDT into interrupt mode */
  /* Set shortest prescaler(time-out) value = 2048 cycles (~16 ms) */
  WDTCSR = _BV(WDIE);

  sei();

  Timer1.initialize(30000); // set a timer of length somewhat longer than watchdog length
}


// Rotate bits to the left
// https://en.wikipedia.org/wiki/Circular_shift#Implementing_circular_shifts
byte rotl(const byte value, int shift) {
  if ((shift &= sizeof(value) * 8 - 1) == 0)
    return value;
  return (value << shift) | (value >> (sizeof(value) * 8 - shift));
}


// call often, fills out the random array
void PROBABLYRANDOMCollect()
{
  if (PROBABLYRANDOM.sample_waiting) {
    PROBABLYRANDOM.sample_waiting = false;

    PROBABLYRANDOM.result = rotl(PROBABLYRANDOM.result, 1); // Spread randomness around
    PROBABLYRANDOM.result ^= PROBABLYRANDOM.sample; // XOR preserves randomness

    PROBABLYRANDOM.current_bit++;
    if (PROBABLYRANDOM.current_bit > 7)
    {
      PROBABLYRANDOM.current_bit = 0;
      PROBABLYRANDOM.random[PROBABLYRANDOM.randomptr] = PROBABLYRANDOM.result;
      PROBABLYRANDOM.randomptr++;
      if (PROBABLYRANDOM.randomptr == MAXRANDOMSAMPLES)
      {
        PROBABLYRANDOM.randomptr = 0;
      }

      if (PROBABLYRANDOM.samples < MAXRANDOMSAMPLES)
      {
        PROBABLYRANDOM.samples++;
      }
    }
  }
}


void ARCFOURPrint()
{
  byte i;

  i = 0;
  Serial.print(F("IV: "));
  do
  {
    Serial.print(ARCFOUR.IV[i++]);
    Serial.print(' ');
  } while (i < 10);
  Serial.print(F("\x0d\x0a"));


  i = 0;
  Serial.print(F("S: "));
  do
  {
    Serial.print(ARCFOUR.State[i++]);
    Serial.print(' ');
  } while (i);
  Serial.print(F("\x0d\x0a"));
}


void ARCFOURClear()
{
  ARCFOUR.i = 0;
  ARCFOUR.j = 0;
  ARCFOUR.KeyEnd = 0;
  do
  {
    ARCFOUR.Key[ARCFOUR.i] = 0;
    ARCFOUR.State[ARCFOUR.i] = 0;
    ARCFOUR.i++;
  } while (ARCFOUR.i);
}


void ARCFOURSetKey(byte *PassPhrase, byte *IV, byte Loops)
{
  byte i = 0;
  byte j = 0;
  byte c;

  Serial.print(F("key:\x0d\x0a"));

  do
  {
    c = PassPhrase[i];

    Serial.print(i);
    Serial.print(' ');
    Serial.print((char)c);
    Serial.print(F("\x0a\x0d"));

    ARCFOUR.Key[i] = c;

    if (c != 0)
    {
      i++;
    }

  } while ((c != 0) && (i < 246));

  Serial.print(F("iv:\x0a\x0d"));

  do
  {
    Serial.print(i);
    Serial.print(' ');
    Serial.print(IV[j]);
    Serial.print(F("\x0a\x0d"));

    ARCFOUR.Key[i] = IV[j];
    i++;
    j++;
  } while (j < 10);

  ARCFOUR.KeyEnd = i;

  Serial.print(F("ke:"));
  Serial.print(i);
  Serial.print(F("\x0a\x0d"));

  ARCFOUR.n = Loops;
}


void ARCFOURAppendIVtoKey()
{
  for (byte i = 0; i < 10; i++)
  {
    ARCFOUR.Key[ARCFOUR.KeyEnd++] = ARCFOUR.IV[i];
  }
}


void ARCFOURSwap(byte i, byte j)
{
  byte t = ARCFOUR.State[i];
  ARCFOUR.State[i] = ARCFOUR.State[j];
  ARCFOUR.State[j] = t;
}


void ARCFOURInit()
{
  byte n = 0;

  ARCFOUR.i = 0;
  ARCFOUR.j = 0;

  do
  {
    ARCFOUR.State[ARCFOUR.i] = ARCFOUR.i;
    ARCFOUR.i++;
  } while (ARCFOUR.i);

  //i=0

  for (byte i = 0; i < ARCFOUR.n; i++)
  {
    n = 0;

    do
    {
      ARCFOUR.j += ARCFOUR.State[ARCFOUR.i] + ARCFOUR.Key[n];
      ARCFOURSwap(ARCFOUR.i, ARCFOUR.j);

      ARCFOUR.i++;
      n++;

      if (n == ARCFOUR.KeyEnd)
      {
        n = 0;
      }
    } while (ARCFOUR.i);
  }

  //i=0

  do
  {
    ARCFOUR.Key[ARCFOUR.i] = 0;
    ARCFOUR.i++;
  } while (ARCFOUR.i);

  ARCFOUR.i = 0;
  ARCFOUR.j = 0;
  ARCFOUR.KeyEnd = 0;
}


byte ARCFOUREncrypt(byte in)
{
  byte out;
  byte n;

  ARCFOUR.i++;
  ARCFOUR.j += ARCFOUR.State[ARCFOUR.i];
  ARCFOURSwap(ARCFOUR.i, ARCFOUR.j);
  n = ARCFOUR.State[ARCFOUR.i] + ARCFOUR.State[ARCFOUR.j];

  out = ARCFOUR.State[n] ^ in;

  return out;
}


void DoARCFOUR(byte *file)
{
  byte v;
  byte i = 0;

  do
  {
    v = file[i++];

    if (v)
    {
      Serial.print((char)ARCFOUREncrypt(v));
    }
  } while (v);

  Serial.print(F("\x0d\x0a"));
}


void CipherSaberTests()
{
  byte Pass[246] = {'a', 's', 'd', 'f', 'g', 0};

  byte IV[10] = {0x6f, 0x6d, 0x0b, 0xab, 0xf3, 0xaa, 0x67, 0x19, 0x03, 0x15};

  byte file[] = {0x30, 0xed, 0xb6, 0x77, 0xca, 0x74, 0xe0, 0x08, 0x9d, 0xd0,
                 0xe7, 0xb8, 0x85, 0x43, 0x56, 0xbb, 0x14, 0x48, 0xe3, 0x7c,
                 0xdb, 0xef, 0xe7, 0xf3, 0xa8, 0x4f, 0x4f, 0x5f, 0xb3, 0xfd,
                 0x00
                };

  byte IV2[10] = {0xba, 0x9a, 0xb4, 0xcf, 0xfb, 0x77, 0x00, 0xe6, 0x18, 0xe3};

  byte file2[] = {0x82, 0xe8, 0xfc, 0xc5, 0xab, 0x98, 0x13, 0xb1, 0xab, 0xc4,
                  0x36, 0xba, 0x7d, 0x5c, 0xde, 0xa1, 0xa3, 0x1f, 0xb7, 0x2f,
                  0xb5, 0x76, 0x3c, 0x44, 0xcf, 0xc2, 0xac, 0x77, 0xaf, 0xee,
                  0x19, 0xad, 0x00
                 };

  // bad data, does not decode
  byte Pass3[246] = {'t', 'e', 's', 't'};
  byte IV3[10] = {0x43, 0x91, 0xe8, 0xec, 0xcd, 0x8b, 0x7d, 0x98, 0x27, 0x45};
  byte file3[] = {0xda, 0xea, 0x54, 0x65, 0x00};

  //Serial.println(sizeof(Pass));

  ARCFOURSetKey(Pass, IV, 1);
  ARCFOURInit();
  ARCFOURPrint();
  DoARCFOUR(file);

  ARCFOURSetKey(Pass, IV2, 10);
  ARCFOURInit();
  ARCFOURPrint();
  DoARCFOUR(file2);

  ARCFOURSetKey(Pass, IV, 1);
  ARCFOURInit();
  ARCFOURPrint();
  DoARCFOUR(file);

  ARCFOURSetKey(Pass3, IV3, 1);
  ARCFOURInit();
  ARCFOURPrint();
  DoARCFOUR(file3);

  ARCFOURClear();
  ARCFOURPrint();
}


//
void ShowNewRandom()
{
  if (PROBABLYRANDOM.samples == MAXRANDOMSAMPLES) // show 10 new values
    //if (PROBABLYRANDOM.samples == 1) // show new value propagating in circular queue
  {
    for (byte i = 0; i < MAXRANDOMSAMPLES; i++)
    {
      Serial.print(PROBABLYRANDOM.random[i]);
      Serial.print(F(" "));
    }
    Serial.print(F("\x0d\x0a"));
    PROBABLYRANDOM.samples = 0;
  }
}


//call often, returns 1 when IV has been initialized
byte FillARCFOURIV()
{
  byte v = 0;

  static byte collecting = false;

  if (collecting == false)
  {
    PROBABLYRANDOM.samples = 0;
    collecting = true;
  }
  else
  {
    if (PROBABLYRANDOM.samples == MAXRANDOMSAMPLES) // show 10 new values, a full IV
    {
      for (byte i = 0; i < MAXRANDOMSAMPLES; i++)
      {
        ARCFOUR.IV[i] = PROBABLYRANDOM.random[i];
      }

      v = 1;
      collecting = false;
      PROBABLYRANDOM.samples = 0;
    }
  }

  return v;
}


void ShowSerial()
{
  byte k = Serial.read();

  if (k != 255)
  {
    Serial.print(k);
    Serial.print(F("\x0a\x0d"));
  }
}


void PrintHex(byte val)
{
  byte nibble = (val & 0xf0) >> 4;
  Serial.print((char)(nibble > 9 ? 'A' - 10 + nibble : '0' + nibble));
  nibble = val & 0x0f;
  Serial.print((char)(nibble > 9 ? 'A' - 10 + nibble : '0' + nibble));
}


byte HexToByte(byte *HexByte)
{
  byte hex = 0;
  byte nibble = 0;
  byte weigh = 0x10;

  for (byte i = 0; i < 2; i++)
  {
    if ((HexByte[i] >= '0') && (HexByte[i] <= '9'))
    {
      nibble = HexByte[i] - '0';
    }
    else if (((HexByte[i] | 32) >= 'a') && ((HexByte[i] | 32) <= 'f'))
    {
      nibble = (HexByte[i] | 32) - 'a' + 10;
    }

    hex += nibble * weigh;

    weigh >>= 4;
  }

  return hex;
}


void DoMenu()
{
  byte key = Serial.read();

  if (key == 10)
  {
    key = 13;
  }

  if (key == 13)
  {
    MenuData.ExitCount++;
  }

  if (MenuData.ExitCount == 4)
  {
    MenuData.ExitCount = 0;
    Serial.print(F("\x0a\x0d\x0a\x0d"));

    MenuData.State = 0;
  }

  if ((MenuData.SkipNewline) && (key != 255))
  {
    if (key == 13)
    {
      return;
    }
    else
    {
      MenuData.SkipNewline = 0;
    }
  }

  switch (MenuData.State)
  {
    case 0:
      {
        Serial.print(F("(D)ecrypt as HEX, (d)ecrypt as ASCII or (E)ncrypt?: "));
        MenuData.ExitCount = 0;

        MenuData.State = 1;
        break;
      }

    case 1:
      {
        MenuData.ExitCount = 0;

        if ((key | 32) == 'e')
        {
          FillARCFOURIV();
          Serial.print(F("E\x0a\x0d"));
          MenuData.SkipNewline = 1;

          MenuData.NextState = 8;
          MenuData.State = 2;
        }

        if ((key | 32) == 'd')
        {
          Serial.print((char)key);
          Serial.print(F("\x0a\x0d"));
          MenuData.SkipNewline = 1;

          if (key == 'd')
          {
            MenuData.DecodeAsHex = 0;
          }
          else
          {
            MenuData.DecodeAsHex = 1;
          }

          MenuData.NextState = 6;
          MenuData.State = 2;
        }

        break;
      }

    case 2:
      {
        Serial.print(F("Enter Passphrase: "));
        MenuData.ExitCount = 0;
        ARCFOUR.KeyEnd = 0;

        MenuData.State = 3;
        break;
      }

    case 3:
      {
        MenuData.ExitCount = 0;

        if (key != 255)
        {
          if (key == 13)
          {
            Serial.print(F("*********************\x0a\x0d"));
            MenuData.SkipNewline = 1;

            MenuData.State = 4;
          }
          else
          {
            ARCFOUR.Key[ARCFOUR.KeyEnd++] = key;
          }
        }

        break;
      }

    case 4:
      {
        Serial.print(F("How Many Rounds: "));
        MenuData.ExitCount = 0;
        MenuData.Rounds[0] = '0';
        MenuData.Rounds[1] = '0';

        MenuData.State = 5;
        break;
      }

    case 5:
      {
        MenuData.ExitCount = 0;

        if (key != 255)
        {
          if (key == 13)
          {
            ARCFOUR.n = (MenuData.Rounds[0] - '0') * 10 + (MenuData.Rounds[1] - '0');
            Serial.print(ARCFOUR.n);
            Serial.print(F("\x0a\x0d"));
            MenuData.SkipNewline = 1;

            MenuData.State = MenuData.NextState;
          }
          else
          {
            if ((key >= '0') && (key <= '9'))
            {
              MenuData.Rounds[0] = MenuData.Rounds[1];
              MenuData.Rounds[1] = key;
            }
          }
        }

        break;
      }

    // START OF DECODE LOGIC

    case 6:
      {
        Serial.print(F("Enter Text to Decode (send empty lines to end):\x0a\x0d"));
        MenuData.ExitCount = 0;
        MenuData.IVRead = 0;
        MenuData.HexRead = 0;
        MenuData.PrintCount = 0;

        MenuData.State = 7;
        break;
      }

    case 7:
      {
        if (key != 255)
        {
          if (key == 13)
          {
            MenuData.ExitCount = 0;
            MenuData.SkipNewline = 1;
          }
          else
          {
            MenuData.ExitCount = 0;

            if (((key >= '0') && (key <= '9')) || (((key | 32) >= 'a') && ((key | 32) <= 'f')))
            {
              MenuData.Hex[MenuData.HexRead++] = key;

              if (MenuData.HexRead == 2)
              {
                if (MenuData.IVRead < 10)
                {
                  ARCFOUR.IV[MenuData.IVRead++] = HexToByte(MenuData.Hex);
                  if (MenuData.IVRead == 10)
                  {
                    ARCFOURAppendIVtoKey();
                    ARCFOURInit();
                  }
                }
                else
                {
                  MenuData.PrintCount++;

                  if (MenuData.DecodeAsHex == 0)
                  {
                    Serial.print((char)ARCFOUREncrypt(HexToByte(MenuData.Hex)));

                    if (MenuData.PrintCount > 40)
                    {
                      MenuData.PrintCount = 0;
                      Serial.print(F("\x0a\x0d"));
                    }
                  }
                  else
                  {
                    PrintHex(ARCFOUREncrypt(HexToByte(MenuData.Hex)));

                    if (MenuData.PrintCount > 20)
                    {
                      MenuData.PrintCount = 0;
                      Serial.print(F("\x0a\0x0d"));
                    }
                    else
                    {
                      Serial.print(F(" "));
                    }
                  }
                }

                MenuData.HexRead = 0;
              }
            }
          }
        }

        break;
      }

    // START of ENCODE LOGIC

    case 8:
      {
        MenuData.ExitCount = 0;

        if (FillARCFOURIV())
        {
          MenuData.State = 9;
        }
        break;
      }

    case 9:
      {
        MenuData.PrintIV = 1;
        MenuData.PrintCount = 0;
        MenuData.ExitCount = 0;

        ARCFOURAppendIVtoKey();
        ARCFOURInit();

        MenuData.State = 10;
        break;
      }

    case 10:
      {
        Serial.print(F("Enter Text to Encode (send empty lines to end):\x0a\x0d"));
        MenuData.ExitCount = 0;

        MenuData.State = 11;
        break;
      }

    case 11:
      {
        if (key != 255)
        {
          if (key == 13)
          {
            MenuData.ExitCount = 0;
            MenuData.SkipNewline = 1;
          }
          else
          {
            MenuData.ExitCount = 0;

            if (MenuData.PrintIV)
            {
              MenuData.PrintIV = 0;
              MenuData.PrintCount = 10;

              for (byte i = 0; i < 10; i++)
              {
                PrintHex(ARCFOUR.IV[i]);
                Serial.print(F(" "));
              }
            }

            MenuData.PrintCount++;
            PrintHex(ARCFOUREncrypt(key));
            if (MenuData.PrintCount > 20)
            {
              MenuData.PrintCount = 0;
              Serial.print(F("\x0a\0x0d"));
            }
            else
            {
              Serial.print(F(" "));
            }
          }
        }

        break;
      }
  }
}


void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);

  Serial.print(F("Arduino CipherSaber-2 20191201\x0d\x0a\x0d\x0a"));

  PROBABLYRANDOMSetup();

  //CipherSaberTests();
}


void loop() {
  // put your main code here, to run repeatedly:

  PROBABLYRANDOMCollect(); // call often, exits fast
  DoMenu();
}
