
struct ARCFOUR_t     /*xls*/
{
  byte Key[256];
  byte KeyEnd;
  byte State[256];
  byte i;
  byte j;
  byte n;
}
ARCFOUR;


void ARCFOURPrint()
{
  byte i = 0;

  Serial.print(F("S: "));

  do
  {
    Serial.print(ARCFOUR.State[i++]);
    Serial.print(' ');
  } while (i);

  Serial.print(F("\x0d\x0a"));
}



void ARCFOURClear()
{
  ARCFOUR.i = 0;
  ARCFOUR.j = 0;
  ARCFOUR.KeyEnd = 0;
  do
  {
    ARCFOUR.Key[ARCFOUR.i] = 0;
    ARCFOUR.State[ARCFOUR.i] = 0;
    ARCFOUR.i++;
  } while (ARCFOUR.i);
}

void ARCFOURSetKey(byte *PassPhrase, byte *IV, byte Loops)
{
  byte i = 0;
  byte j = 0;
  byte c;

  Serial.print(F("key:\x0d\x0a"));

  do
  {
    c = PassPhrase[i];

    Serial.print(i);
    Serial.print(' ');
    Serial.println((char)c);

    ARCFOUR.Key[i] = c;

    if (c != 0)
    {
      i++;
    }

  } while ((c != 0) && (i < 246));

  Serial.print(F("iv:\x0a\x0d"));

  do
  {
    Serial.print(i);
    Serial.print(' ');
    Serial.println(IV[j]);

    ARCFOUR.Key[i] = IV[j];
    i++;
    j++;
  } while (j < 10);

  //i--;

  ARCFOUR.KeyEnd = i;

  Serial.print(F("ke:"));
  Serial.println(i);

  ARCFOUR.n = Loops;
}


void ARCFOURSwap(byte i, byte j)
{
  byte t = ARCFOUR.State[i];
  ARCFOUR.State[i] = ARCFOUR.State[j];
  ARCFOUR.State[j] = t;
}


void ARCFOURInit()
{
  byte n = 0;

  ARCFOUR.i = 0;
  ARCFOUR.j = 0;

  do
  {
    ARCFOUR.State[ARCFOUR.i] = ARCFOUR.i;
    ARCFOUR.i++;
  } while (ARCFOUR.i);

  //i=0

  for (byte i = 0; i < ARCFOUR.n; i++)
  {
    n = 0;

    do
    {
      ARCFOUR.j += ARCFOUR.State[ARCFOUR.i] + ARCFOUR.Key[n];
      ARCFOURSwap(ARCFOUR.i, ARCFOUR.j);

      ARCFOUR.i++;
      n++;

      if (n == ARCFOUR.KeyEnd)
      {
        n = 0;
      }
    } while (ARCFOUR.i);
  }

  //i=0

  do
  {
    ARCFOUR.Key[ARCFOUR.i] = 0;
    ARCFOUR.i++;
  } while (ARCFOUR.i);

  ARCFOUR.i = 0;
  ARCFOUR.j = 0;
  ARCFOUR.KeyEnd = 0;
}


byte ARCFOUREncrypt(byte in)
{
  byte out;
  byte n;

  ARCFOUR.i++;
  ARCFOUR.j += ARCFOUR.State[ARCFOUR.i];
  ARCFOURSwap(ARCFOUR.i, ARCFOUR.j);
  n = ARCFOUR.State[ARCFOUR.i] + ARCFOUR.State[ARCFOUR.j];

  out = ARCFOUR.State[n] ^ in;

  return out;
}

void DoARCFOUR(byte *file)
{
  byte v;
  byte i = 0;

  do
  {
    v = file[i++];

    if (v)
    {
      Serial.print((char)ARCFOUREncrypt(v));
    }
  } while (v);

  Serial.print(F("\x0d\x0a"));
}


void readPass()
{

}


void setup() {
  // put your setup code here, to run once:

  byte Pass[246] = {'a', 's', 'd', 'f', 'g', 0};

  byte IV[10] = {0x6f, 0x6d, 0x0b, 0xab, 0xf3, 0xaa, 0x67, 0x19, 0x03, 0x15};

  byte file[] = {0x30, 0xed, 0xb6, 0x77, 0xca, 0x74, 0xe0, 0x08, 0x9d, 0xd0,
                 0xe7, 0xb8, 0x85, 0x43, 0x56, 0xbb, 0x14, 0x48, 0xe3, 0x7c,
                 0xdb, 0xef, 0xe7, 0xf3, 0xa8, 0x4f, 0x4f, 0x5f, 0xb3, 0xfd,
                 0x00
                };

  byte IV2[10] = {0xba, 0x9a, 0xb4, 0xcf, 0xfb, 0x77, 0x00, 0xe6, 0x18, 0xe3};

  byte file2[] = {0x82, 0xe8, 0xfc, 0xc5, 0xab, 0x98, 0x13, 0xb1, 0xab, 0xc4,
                  0x36, 0xba, 0x7d, 0x5c, 0xde, 0xa1, 0xa3, 0x1f, 0xb7, 0x2f,
                  0xb5, 0x76, 0x3c, 0x44, 0xcf, 0xc2, 0xac, 0x77, 0xaf, 0xee,
                  0x19, 0xad, 0x00
                 };

  Serial.begin(9600);


  Serial.println(sizeof(Pass));

  ARCFOURSetKey(Pass, IV, 1);
  ARCFOURInit();
  ARCFOURPrint();
  DoARCFOUR(file);

  ARCFOURSetKey(Pass, IV2, 10);
  ARCFOURInit();
  ARCFOURPrint();
  DoARCFOUR(file2);

  //ARCFOURSetKey(Pass, IV, 1);
  //ARCFOURInit();
  //ARCFOURPrint();
  //DoARCFOUR(file);

  ARCFOURClear();
  ARCFOURPrint();
}


void loop() {
  // put your main code here, to run repeatedly:

}