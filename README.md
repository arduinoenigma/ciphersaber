# CipherSaber

An implementation of CipherSaber / ARCFOUR / RC4 for the Arduino platform.

CipherSaberDemo.ino shows that the implementation is correct by decoding test vectors

CipherSaber.ino runs a full implementation accessible through serial port. It encodes and decodes CipherSaber files in hex format.
It uses timer jitter to generate random numbers for the Initialization Vector.

http://ciphersaber.gurus.org/